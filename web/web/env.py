"""
DO NOT COMMIT
"""

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

SECRET_KEY = 'me4137ve@%2_6%*!av)b5(@b8*=l+@pb1il3usna_&g=z#ne4d'

CLEARBIT_KEY = 'sk_69947118cafbba1053f409f576dc9df8'
HUNTER_KEY = 'd6ef3436c44438da406509c35df0def7f691aba8'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
