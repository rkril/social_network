from rest_framework import serializers

from posts.models import Post, Like


class PostSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author = serializers.CharField(source="user.email", read_only=True)
    created_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'author', 'like_count', 'created_at', 'user')
        extra_kwargs = {
            'text': {'write_only': True}
        }


class PostDetailSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author = serializers.CharField(source="user.email", read_only=True)
    created_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)
    who_likes = serializers.StringRelatedField(many=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'author', 'created_at', 'like_count', 'user', 'who_likes')


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('user', 'post',)
