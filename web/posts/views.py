from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from posts.models import Post, Like
from posts.serializers import PostSerializer, PostDetailSerializer, LikeSerializer


class PostViewSet(viewsets.GenericViewSet,
                  mixins.ListModelMixin,
                  mixins.CreateModelMixin):
    serializer_class = PostSerializer
    queryset = Post.objects.all().order_by('-created_at')
    permission_classes = (IsAuthenticated,)


class PostDetailViewSet(viewsets.GenericViewSet,
                        mixins.UpdateModelMixin,
                        mixins.RetrieveModelMixin):
    serializer_class = PostDetailSerializer
    queryset = Post.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Post.objects.all()


class PostLikeViewSet(viewsets.GenericViewSet,
                      mixins.UpdateModelMixin,
                      mixins.RetrieveModelMixin):
    serializer_class = LikeSerializer
    queryset = Post.objects.all()
    permission_classes = (IsAuthenticated,)

    @action(detail=True, methods=['put', 'delete'], url_name="like", url_path="like")
    def do_like(self, request, pk=None):
        user = request.user
        if request.method == "PUT":
            serializer = LikeSerializer(data={'post': pk, 'user': user.id}, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response(data=serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        elif request.method == "DELETE":
            like = Like.objects.filter(post_id=pk, user=user).first()
            if like:
                like.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
