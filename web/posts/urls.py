from rest_framework.routers import DefaultRouter

from .views import PostViewSet, PostDetailViewSet, PostLikeViewSet

router = DefaultRouter()

router.register('posts', PostViewSet, base_name='posts')
router.register('posts', PostDetailViewSet, base_name='posts')
router.register('posts', PostLikeViewSet, base_name='posts')

# urlpatterns = []
#
# urlpatterns += router.urls
