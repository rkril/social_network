from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class AbstractModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Post(AbstractModel):
    title = models.CharField(max_length=100, null=False, blank=False)
    text = models.TextField()
    who_likes = models.ManyToManyField(User, through='Like', related_name='who_likes')

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def like_count(self):
        return self.like_set.count()

    def __str__(self):
        return f'{self.user.email}: {self.text}'


class Like(AbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user.email}: {self.post.text}'
