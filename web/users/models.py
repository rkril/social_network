from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from users.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('First name'), max_length=60, blank=True, null=True)
    last_name = models.CharField(_('Last name'), max_length=60, blank=True, null=True)
    full_name = models.CharField(_('Full name'), max_length=120, blank=True, null=True)
    email_validity = models.IntegerField(_('hunter.io score'), null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    clearbit_data = models.TextField(null=True)
    hunter_data = models.TextField(null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()
