from django.conf.urls import url
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from users.views import UserListViewSet, UserDetailViewSet, UserSignupViewSet

router = routers.DefaultRouter()
router.register('users', UserListViewSet, base_name='users')
router.register('users', UserDetailViewSet, base_name='users')
router.register('signup', UserSignupViewSet, base_name='signup')

urlpatterns = [
    url(r'^auth/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r'^auth/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
]
