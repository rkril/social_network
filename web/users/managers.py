from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _

from users.stuff import enrich_clearbit, enrich_hunter


class UserManager(BaseUserManager):

    @staticmethod
    def enrich_data(extra_fields, email):

        domain = email.split('@')[1]

        # enrich data if user is not bot
        if domain not in ('bot.com',):
            clearbit_data = enrich_clearbit(email)
            hunter_data = enrich_hunter(email)
            extra_fields.update(clearbit_data)
            extra_fields.update(hunter_data)

        return extra_fields

    def create_user(self, email, password, **extra_fields):

        if not email:
            raise ValueError(_('The Email must be set'))

        extra_fields = self.enrich_data(extra_fields, email)
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)
