import clearbit
from pyhunter import PyHunter
import json

from django.conf import settings

clearbit.key = settings.CLEARBIT_KEY
hunter = PyHunter(settings.HUNTER_KEY)


def enrich_clearbit(email):
    def fetch_response():
        try:
            ret = clearbit.Enrichment.find(email=email, stream=True)
            return dict(ret)
        except:
            return {}

    full_data = fetch_response()
    person = full_data.get('person', {}) or {}
    result = {
        'clearbit_data': json.dumps(full_data),
        'first_name': person.get('name', {}).get('givenName'),
        'last_name': person.get('name', {}).get('familyName'),
        'full_name': person.get('name', {}).get('fullName'),
    }
    return result


def enrich_hunter(email):
    def fetch_response():
        try:
            ret = hunter.email_verifier(email=email)
            return dict(ret)
        except:
            return {}

    full_data = fetch_response()
    result = {
        'hunter_data': json.dumps(full_data),
        'email_validity': full_data.get('score', 0)
    }
    return result
