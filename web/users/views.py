from rest_framework import viewsets, permissions, mixins
from users.serializers import UserListSerializer, UserDetailSerializer, UserSignupSerializer

from django.contrib.auth import get_user_model

User = get_user_model()


class UserListViewSet(viewsets.GenericViewSet,
                      mixins.ListModelMixin):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserListSerializer
    permission_classes = (permissions.IsAuthenticated,)


class UserDetailViewSet(viewsets.GenericViewSet,
                        mixins.RetrieveModelMixin,
                        mixins.CreateModelMixin,
                        mixins.UpdateModelMixin):
    model = User
    serializer_class = UserDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)

    queryset = User.objects.all()


class UserSignupViewSet(viewsets.GenericViewSet,
                        mixins.CreateModelMixin):
    model = User
    serializer_class = UserSignupSerializer
    permission_classes = (permissions.AllowAny,)
