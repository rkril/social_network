import requests
import random

from common import log, config

URI = config["uri"]


def signup(email, password):
    payload = {
        "email": email,
        "password": password
    }

    res = requests.post(url=f'{URI}signup/', json=payload)
    if res.status_code != 201:
        log.error(f'Failed to signup: {email} with error: {res.status_code}')


def get_token(email, password):
    payload = {
        'email': email,
        'password': password
    }
    res = requests.post(url=f'{URI}auth/token/', json=payload)
    if res.status_code != 200:
        log.error(f'Failed to login: {email} with error: {res.status_code}')

    tokens = res.json()
    return tokens['access']


def get_headers(token):
    return {'Authorization': f'Bearer {token}'}


def create_posts(token, posts):
    headers = get_headers(token)

    for title, text in posts:
        payload = {
            "title": title,
            "text": text
        }
        res = requests.post(url=f'{URI}posts/', json=payload, headers=headers)
        if res.status_code != 201:
            log.error(f'Failed to create post with code {res.status_code}')


def get_post_ids(token):
    headers = get_headers(token)
    res = requests.get(url=f'{URI}posts/', headers=headers)
    data = res.json()
    random.shuffle(data)
    ids = [d['id'] for d in data]
    max_likes = config['max_likes_per_user']
    likes_per_user = random.randint(max_likes // 2, max_likes)
    likes_per_user = min(len(ids), likes_per_user)
    return ids[:likes_per_user]


def like_posts(token, post_ids):
    headers = get_headers(token)

    for id in post_ids:
        res = requests.put(url=f'{URI}posts/{id}/like/', headers=headers)
        if res.status_code != 201:
            log.error(f'Failed to like post {id} with code {res.status_code}')
