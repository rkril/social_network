import logging
import json


log = logging.getLogger('bot')
log.setLevel(logging.DEBUG)
fh = logging.FileHandler('bot.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
log.addHandler(fh)
log.addHandler(ch)


def read_config(filename):
    try:
        with open(filename) as f:
            data = json.load(f)
            return data
    except FileNotFoundError:
        return {}


def get_config(filename='config.json'):
    data = read_config(filename)
    mandatory = {"uri", "number_of_users", "max_posts_per_user", "max_likes_per_user"}

    diff = mandatory - set(data)
    if not diff:
        return data
    else:
        err = f'No params {diff}'
        log.error(err)
        raise ValueError(err)


config = get_config()



