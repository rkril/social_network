from collections import defaultdict

from common import log, config
from fake import generate_users, generate_posts
from interaction import signup, get_token, create_posts, like_posts, get_post_ids


def run():
    users = generate_users()
    [signup(email, passwd) for email, passwd in users]
    tokens = [get_token(email, passwd) for email, passwd in users]

    report_data = defaultdict(int)
    report_data['users'] = len(users)
    for t in tokens:
        user_posts = generate_posts()
        create_posts(t, user_posts)
        report_data['posts'] += len(user_posts)

        post_ids = get_post_ids(t)
        like_posts(t, post_ids)
        report_data['likes'] += len(post_ids)

    report = '\n\t'.join([f'{k}: {v}' for k, v in report_data.items()])
    log.critical(f'Done!\n  Processed:\n\t{str(report)}')


if __name__ == '__main__':
    from pprint import pprint

    run()
    # token = get_token('roman.kril@gmail.com', 'roman.kril')
    # print(get_post_ids(token))
