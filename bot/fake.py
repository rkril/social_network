from faker import Faker
import random

from common import config

fake = Faker()


def generate_user():
    name = fake.name()
    normalized_name = '.'.join(name.split()).lower()
    email = f'{normalized_name}@bot.com'
    password = normalized_name
    return email, password


def generate_post():
    title = fake.text(30)
    text = fake.text(300)
    return title, text


def generate_users():
    return [generate_user() for _ in range(config['number_of_users'])]


def generate_posts():
    posts_per_user = random.randint(1, config['max_posts_per_user'])
    return [generate_post() for _ in range(posts_per_user)]
