# Social Network
Sample of REST API for Social network.


## Usage
Download or clone the project.  
Run via [docker-compose](https://docs.docker.com/compose/install/).
```
$ git clone https://bitbucket.org/rkril/social_network/
$ cd social_network
$ docker-compose up --build
```
Superuser account: admin@admin.com, password: admin   
    

Run the bot (requires [python 3.6+](https://www.python.org/downloads/release/python-360/), 
[requests](http://docs.python-requests.org/en/master/), 
[faker](https://faker.readthedocs.io/en/master/))
```
$ cd bot
$ python main.py
```

## Access points
* **Signup** [POST] http://0.0.0.0:8000/signup/ 
* **Token Get** [POST] http://0.0.0.0:8000/auth/token/  
* **Token Refresh** [POST] http://0.0.0.0:8000/auth/token/refresh/ 
* **Users all** [GET] http://0.0.0.0:8000/users/ 
* **Users specific** [GET, PUT, PATCH] http://0.0.0.0:8000/users/{id}  
* **Posts all** [GET] http://0.0.0.0:8000/posts/ 
* **Post create** [POST] http://0.0.0.0:8000/posts/ 
* **Posts specific** [GET, PUT, PATCH] http://0.0.0.0:8000/users/{id}
* **Post like** [PUT] http://0.0.0.0:8000/posts/{id}/like  
* **Post dislike** [DELETE] http://0.0.0.0:8000/posts/{id}/like  

You can explore the API via  [Swagger](https://django-rest-swagger.readthedocs.io/en/latest/). 
It available on http://127.0.0.1:8000/swagger/  
Schema for [Postman](https://www.getpostman.com/) also attached (```collection.json```)

## Built With
* [Docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/install/)

#### Backend
* [Django](https://www.djangoproject.com/) 
    * [Django Rest Framework](https://www.django-rest-framework.org/) - REST API development toolkit in Django
    * [django-rest-framework-simplejwt](https://github.com/davesque/django-rest-framework-simplejwt) - JWT auth plugin
    * [PyHunter](https://github.com/VonStruddle/PyHunter) - hunter.io client for Python
    * [clearbit-python](https://github.com/clearbit/clearbit-python/tree/master/clearbit) - clearbit client for Python

#### Automated bot
* [requests](http://docs.python-requests.org/en/master/) - http client lib
* [faker](https://faker.readthedocs.io/en/master/) - fake data generator

## Authors
* **Roman Kril**